# New Hire Shourtcut
# This script will generate URL shortcut on Public Desktop folder if user folder creation time is less than $cutofftime

# Variables
# ---------
$ShortcutExists = Test-Path -Path "C:\Users\Public\Desktop\Shortcut_Name.lnk*" -PathType Leaf
$logFile = "C:\Windows\Temp\NewHireShorcut.txt"
# User folder creation date in ( days ago )
$Usercreated = ((Get-Date) - (Get-Childitem C:\Users\($env:username)).CreationTime).Days
# days time limit until user will get the shortcut deployed
$cutofftime = "2"
# ---------

# Start the logging 
Start-Transcript $logFile
#Download the file
Invoke-WebRequest -Uri "https://path/to/ico/file.ico" -OutFile ( New-Item -Path "C:\Temp\Icon.ico" -Force ) -ErrorAction SilentlyContinue

# Create or not to create shortcut
# --------------------------------
# if shorcut doesnt already exist, then it checks if user folder creation time is less than $cuttofftime days old, if yes then it installs the shortcut
If (-not ($ShortcutExists)) {
    Write-Host "Shortcut doesnt exist. Checking if userfolder is newer then $cutofftime days"
    If ($Usercreated -lt $cutofftime) {
        Write-Host "User folder created less than $cutofftime days ago. Creating shortcut"
        $Shell = New-Object -ComObject WScript.Shell
        $Favorite = $Shell.CreateShortcut($env:SYSTEMDRIVE + "\Users\Public\Desktop\Shortcut_Name.lnk")
        $Favorite.TargetPath = "https://target/site/path";
        $Favorite.IconLocation = "C:\Temp\Icon.ico";
        $Favorite.Save()
        Write-Host "Shortcut Saved"
    }
    # if user folder creation time is over $cutofftime, nothing happens
    else {
        Write-Host "User folder was created more than $cutofftime days ago. Skipping..."
    }
}
# if shorcut already exists then no actiona taken
else {
    Write-Host "Shortcut already exists"
}

# Stop logging 
Stop-Transcript